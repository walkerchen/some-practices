#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>
#include <math.h>
#include <memory.h>
#include <assert.h>

int main(void)
{
	int iA=0;
	int iB=0;

	printf("Assert example\n");

	assert(iA!=0);

	iB = 89/iA;

	printf("iB=%d\n", iB);

	printf("Assert example ended.\n");

	return 0;
}
