#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include <memory.h>

//typedef struct BSTreeNodeStruc BSTreeNode;

typedef struct BSTreeNodeStruc
{
	int id;
	int value;
	struct BSTreeNodeStruc *left;
	struct BSTreeNodeStruc *right;
}BSTreeNode;

void printNode(BSTreeNode *node)
{
	printf("Node[%d] = %d", node->id, node->value);
	if(node->left!=NULL)
		printf(", left node=[%d]", node->left->id);
	if(node->right!=NULL)
		printf(", right node=[%d]", node->right->id);
	printf("\n");
}

void insertNode(BSTreeNode **tree, BSTreeNode *node, bool ordered)
{
	// Root node
	if(*tree==NULL)
	{
		*tree = node;
		return;
	}

	if(ordered)
	{
		if( (*tree)->value >node->value )
			insertNode(&((*tree)->left), node, ordered);
		else 
			insertNode(&((*tree)->right), node, ordered);
	}
	else
	{
		if(rand()%2==0)
			insertNode(&((*tree)->left), node, ordered);
		else 
			insertNode(&((*tree)->right), node, ordered);
	}
}

BSTreeNode* load(int size)
{
	if(size<1) return NULL;

	BSTreeNode *head=NULL, *node=NULL;

	srand(time(NULL));
	for(int i=0; i<size; i++)
	{
		int value = rand() % 30;

		node = (BSTreeNode *)malloc(sizeof(BSTreeNode));
		node->id = i;
		node->value = value;
		node->left = node->right = NULL;

		//printNode(node);
		insertNode(&head, node, true);
	}

	return head;
}

void dump(BSTreeNode *node)
{
	if(node==NULL) return;
	printNode(node);
	dump(node->left);
	dump(node->right);
}

void printBinaryTree(BSTreeNode *root, int level)
{
    if(root==NULL)
         return;
    printBinaryTree(root->right, level+1);
    if(level!=0)
    {
        for(int i=0; i<level-1; i++)
            printf("|\t");
        printf("|-------%d\n", root->value);
    }
    else
        printf("%d",root->id);
    printBinaryTree(root->left, level+1);
}

char buf[512];

void printTree(BSTreeNode *node, bool isRight, char *indent)
{
	if(node==NULL) return;

    if(node->right) 
    {
    	sprintf(buf, "%s%s", indent, (isRight ? "        " : " |      "));
        printTree(node->right, true, buf);
    }
    printf("%s", indent);
    if (isRight) 
    {
    	printf(" /");
    } else 
    {
   		printf(" \\");
    }
    printf("----- ");
    printf("%d\n ", node->id);
    
    if(node->left) 
    {
    	sprintf(buf, "%s%s", indent, (isRight ? " |      " : "        "));
    	printTree(node->left, false, buf);
    }
}

void dumpNode(BSTreeNode *node)
{
	if(node->right)
		printTree(node->right, true, (char *)"");
    printf("%d\n ", node->value);
	if(node->left)
		printTree(node->left, false, (char *)"");
}

char *mkrndstr(size_t length) 
{ 
	char *randomString;

	if (length) 
	{
    	randomString = (char *)malloc(length*2 +1); // sizeof(char) == 1, cf. C99

    	if (randomString) 
    	{
	        for (int n = 0;n < length;n+=2) 
	        {        
    	        randomString[n] = '|';
    	        randomString[n+1] = ' ';
        	}

        	randomString[length*2] = '\0';
    	}
	}

	return randomString;
}

void dtree(BSTreeNode *root, char *prefix, int level) 
{
    if (root == NULL) 
    {
    	//printf("%s%s\n", prefix, "+- <null>");
    	return;
    }

    printf("%s%s%d\n", prefix, "+- ", root->value);
    dtree(root->left, mkrndstr(level+1), level+1);
    memset(buf, 512, NULL);
    sprintf(buf, "%s%s", prefix, "| ");
    dtree(root->right, mkrndstr(level+1), level+1);
}

int _print_t(BSTreeNode *tree, int is_left, int offset, int depth, char s[20][255])
{
    char b[20];
    int width = 5;

    if (!tree) return 0;

    sprintf(b, "(%d)", tree->id);

    int left  = _print_t(tree->left,  1, offset,                depth + 1, s);
    int right = _print_t(tree->right, 0, offset + left + width, depth + 1, s);

#ifdef COMPACT
    for (int i = 0; i < width; i++)
        s[depth][offset + left + i] = b[i];

    if (depth && is_left) {

        for (int i = 0; i < width + right; i++)
            s[depth - 1][offset + left + width/2 + i] = '-';

        s[depth - 1][offset + left + width/2] = '.';

    } else if (depth && !is_left) {

        for (int i = 0; i < left + width; i++)
            s[depth - 1][offset - width/2 + i] = '-';

        s[depth - 1][offset + left + width/2] = '.';
    }
#else
    for (int i = 0; i < width; i++)
        s[2 * depth][offset + left + i] = b[i];

    if (depth && is_left) {

        for (int i = 0; i < width + right; i++)
            s[2 * depth - 1][offset + left + width/2 + i] = '-';

        s[2 * depth - 1][offset + left + width/2] = '+';
        s[2 * depth - 1][offset + left + width + right + width/2] = '+';

    } else if (depth && !is_left) {

        for (int i = 0; i < left + width; i++)
            s[2 * depth - 1][offset - width/2 + i] = '-';

        s[2 * depth - 1][offset + left + width/2] = '+';
        s[2 * depth - 1][offset - width/2 - 1] = '+';
    }
#endif

    return left + width + right;
}

int print_t(BSTreeNode *tree)
{
    char s[20][255];
    for (int i = 0; i < 20; i++)
        sprintf(s[i], "%40s", " ");

    _print_t(tree, 0, 0, 0, s);

    for (int i = 0; i < 20; i++)
        printf("%s\n", s[i]);

    return 0;
}

 
/* UTILITY FUNCTIONS */
/* Utility that prints out an array on a line. */
void printArray(int ints[], int len) 
{
  int i;
  for (i=0; i<len; i++) 
  {
    printf("%d ", ints[i]);
  }
  printf("\n");
}  

/* Recursive helper function -- given a node, and an array containing
 the path from the root node up to but not including this node,
 print out all the root-leaf paths.*/
void printPathsRecur(BSTreeNode* node, int path[], int pathLen) 
{
  if (node==NULL) 
    return;
 
  /* append this node to the path array */
  path[pathLen] = node->value;
  pathLen++;
 
  /* it's a leaf, so print the path that led to here  */
  if (node->left==NULL && node->right==NULL) 
  {
    printArray(path, pathLen);
  }
  else
  {
    /* otherwise try both subtrees */
    printPathsRecur(node->left, path, pathLen);
    printPathsRecur(node->right, path, pathLen);
  }
}
 
/*Given a binary tree, print out all of its root-to-leaf
 paths, one per line. Uses a recursive helper to do the work.*/
void printPaths(BSTreeNode *node) 
{
  int path[1000];
  printPathsRecur(node, path, 0);
}
 
void unload(BSTreeNode *node, int side)
{
	if(node==NULL) return;
	printf("Unload node [%d] side=%d\n", node->id, side);
	if(node->left != NULL)
		unload(node->left, 0);
	if(node->right != NULL)
		unload(node->right, 1);
	free(node);
	return;
}

void printSumPath(int *base, int *path)
{
	printf("Found path: ");
	for(int *p=base; p<=path; p++)
		printf("%d ", *p);
	printf("\n");
}

void *sumPath(BSTreeNode *node, int *base, int *path, int sum)
{
	if(node==NULL)
	{
		//printf("-------------\n");
		return NULL;
	}
	sum -= node->value;
	*path = node->value;
	//printf("value=%d, sum=%d\n", node->value, sum);

	if(node->left==NULL && node->right==NULL)
	{
		if(sum==0)
			printSumPath(base, path);
	}
	path++;

	sumPath(node->left, base, path, sum);
	sumPath(node->right, base, path, sum);

	return node;
}

#define PATH_SIZE 128
void printPathWithSum(BSTreeNode *node, int sum)
{
	if(node==NULL) return;

	int path[PATH_SIZE];
	sumPath(node, path, path, sum);
}

void walkTreePre(BSTreeNode *node)
{
	BSTreeNode *p = node, *curr;
	BSTreeNode **stack = (BSTreeNode **)malloc(sizeof(BSTreeNode *)*512);
	BSTreeNode **top = stack;

	//printf("%p %p\n", stack, top);

	while(p)
	{
		curr = p;
		printf("%d ", curr->value);

		if(curr->right)
		{
			// Push right leaf to stack
			*top = curr->right;
			top++;
		}

		if(curr->left)
		{
			// Push left leaf to stack
			*top = curr->left;
			top++;
		}

		// stack is empty
		if(stack==top)
			p = NULL;
		else
		{
			top--;
			p = *top;
		}
	}

	printf("\n");

	free(stack);
}

void walkTreePost(BSTreeNode *node)
{
	BSTreeNode *p = node, *curr;
	BSTreeNode **stack = (BSTreeNode **)malloc(sizeof(BSTreeNode *)*512);
	if(!stack) return;
	BSTreeNode **stackPost = (BSTreeNode **)malloc(sizeof(BSTreeNode *)*512);
	BSTreeNode **stackPostTop = stackPost;
	if(!stackPost)
	{
		free(stack);
		return;
	}
	//printf("%p %p\n", stack, top);

	BSTreeNode **top = stack;
	while(p)
	{
		curr = p;

		// Push current node into post processing queue
		*stackPostTop = curr;
		stackPostTop++;

		if(curr->right)
		{
			// Push right leaf to stack
			*top = curr->right;
			top++;
		}

		if(curr->left)
		{
			// Push left leaf to stack
			*top = curr->left;
			top++;
		}

		// stack is empty
		if(stack==top)
			p = NULL;
		else
		{
			top--;
			p = *top;
		}
	}

	printf("Stack size %ld\n", stackPostTop-stackPost);
	while(*--stackPostTop)
	{
		printf("%d ", (*stackPostTop)->value);
	}

	printf("\n");

	free(stack);
	free(stackPost);
}

int max ( int a, int b )
{
  return a > b ? a : b;
}
 
int height ( BSTreeNode *tree )
{
  if ( tree == 0 )
    return -1;
 
  return max ( height ( tree->left ), height ( tree->right ) ) + 1;
}

void level_order_aux ( BSTreeNode *tree, int level )
{
  if ( tree == 0 )
    return;
 
  if ( level == 0 )
    printf ( "%d ", tree->id );
  else if ( level > 0 ) {
    //puts ( "Recurse left" );
    level_order_aux ( tree->left, level - 1 );
    //puts ( "Recurse right" );
    level_order_aux ( tree->right, level - 1 );
  }
}

void level_order ( BSTreeNode *tree )
{
  for ( int d = 0; d <= height ( tree ); d++ )
    level_order_aux ( tree, d );
}

void levelOrderTraversal(BSTreeNode *root)
{

  BSTreeNode *queue[100] = {(BSTreeNode *)0}; // Important to initialize!

  int size = 0;

  int queue_pointer = 0;
  while(root)
  {
      printf("[%d] ", root->id);
      if(root->left)
      {
        queue[size++] = root->left;
      }

      if(root->right)
      {
        queue[size++] = root->right;
      }
      root = queue[queue_pointer++];    
  }
}

int main()
{
	BSTreeNode *head = NULL;

	head = load(10);
	printf("head=%p\n", head);
	printf("Dumping tree node\n");
	dump(head);
	printBinaryTree(head, 0);
	//dumpNode(head);
	//dtree(head, (char *)" ", 0);
	//print_t(head);
	printPaths(head);
	printPathWithSum(head, 10);

	printf("Walk tree...\n");
	walkTreePre(head);
	walkTreePost(head);

	//level_order(head);
	levelOrderTraversal(head);

	unload(head, 0);

	return(0);
}