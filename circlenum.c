/*
包含size个数字的数字圆圈(索引号idx)间隔step个去除，最终剩下的数字

每一轮：idx%step!=(step-1) 留下，每轮删除outs=Integer(idx/step)，新数组大小=size-outs.

解题思路：
每个数字都不实际删除，而是索引号随着每次数字的删除发生变化，最终满足 idx%step != (step-1) 的数字为最终留下

0 1 2 3 4 5 6 7 8 9 0

每三个去除一个。

下标的最大值maxIdx为 size-outs-1

每一轮，每个数字的下标变为：
0 : 0 >> maxIdx+idx % maxIdx
1 : 1 >> maxIdx+idx % maxIdx
2 : 2 >> -1
3 : 3 >> maxIdx+idx-1(Removed, Integer(idx/step)) % maxIdx
4 : 4 >> maxIdx+idx-1(Removed, Integer(idx/step)) % maxIdx
5 : 5 >> -1
6 : 6 >> maxIdx+idx-2(Removed, Integer(idx/step)) % maxIdx
7 : 7 >> maxIdx+idx-2(Removed, Integer(idx/step)) % maxIdx
8 : 8 >> -1
9 : 9 >> maxIdx+idx-3(Removed, Integer(idx/step)) % maxIdx
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>
#include <math.h>
#include <memory.h>
#include <assert.h>

#define NUM 10
int iNumbers[NUM];

void initNumbers(int *piNum, size_t size)
{
	for(int i=0; i<size; i++)
	{
		//*(piNum+i) = rand()%100;
		*(piNum+i) = i;
	}
}

void printNumbers(int *piNum, size_t size)
{
	for(int i=0; i<size; i++)
	{
		if(i) printf(" ");
		if(*(piNum+i)==-1) 
			printf("%s", ".");
		else 
			printf("%d", *(piNum+i));
	}
	printf("\n");
}

size_t removeNumAt(int *piNum, size_t size, int pos)
{
	/* 只剩一个就不再去掉了 */
	if(size==1) return 1;
	/* 去掉的字符位置非法检查 */
	assert(size>pos);
	for(int i=pos; i<size-1; i++)
	{
		*(piNum+i) = *(piNum+i+1);
	}

	return size-1;
}

size_t removeNumWithStep(int *piNum, size_t size, int step)
{
	/* 只剩一个就不再去掉了 */
	if(size==1) return 1;
	assert(step!=0);
	int newSize = size;
	while(newSize>step)
	{
		int outs = (int)(newSize/step);
		for(int i=0; i<size; i++)
		{
			if(*(piNum+i) == -1) continue;

			if(*(piNum+i)%step == step-1) 
			{
				*(piNum+i) = -1;
			}
			else
				*(piNum+i) = *(piNum+i) - (int)(*(piNum+i)/step);
		}

		newSize -= outs;

		printNumbers(piNum, size);
	}

	return newSize;
}

int main(void)
{
	srand(time(NULL));

	initNumbers(iNumbers, NUM);
	printNumbers(iNumbers, NUM);

	int newSize;
	//newSize = removeNumAt(iNumbers, NUM, 9);
	newSize = removeNumWithStep(iNumbers, NUM, 3);
	//printNumbers(iNumbers, NUM);

	return 0;
}
