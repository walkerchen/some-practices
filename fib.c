#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int doFib(int n);

int doFib(int n)
{
	int v = 0;
	if(n==1) v=1;
	else if(n==2) v=1;
	else v = doFib(n-1) + doFib(n-2);
	//printf("%d, ", v);
	return v;
}

int main(void)
{
	int result = doFib(78);
	printf("%d\n", result);
	return 0;
}