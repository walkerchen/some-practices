#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>
#include <math.h>

inline bool validChar(char c, const char *validChars)
{
	const char *p = validChars;
	while(*p)
	{
		if(c==*p++) return true;
	}
	return false;
}

bool str2int(const char *str, int *result)
{
	const char validChars[] = "01234567890";
	char *charBuf, *charBufPtr;
	bool negative = false;
	int dights = 0;

	if(str==NULL || strlen(str)==0) return false;
	const char *p=str;
	do
	{
		if(validChar(*p, validChars))
		{
			if(dights==0 && p-str>0 && *(p-1)=='-')
				negative = true;
			dights++;
		}
		else if(dights>0)
			break;
	}while(*p++);

	if(dights<=0) return false;

	// MEM: allocate char buffer
	charBuf = (char *)malloc(sizeof(dights)*sizeof(char));
	if(!charBuf) return false;
	charBufPtr = charBuf;

	p = str;
	do
	{
		if(validChar(*p, validChars))
		{
			*charBufPtr = *p;
			charBufPtr++;
		}
		else if(charBufPtr!=charBuf)
			break;
	}while(*p++);

	printf("DEBUG: dights=%d, extract %s, negative=%d\n", dights, charBuf, negative);

	int num = 0;
	for(int i=0; i<dights; i++)
	{
		//printf("%d\n", *(charBuf+i)-'0');
		num += (*(charBuf+i)-'0')*pow(10, dights-i-1);
	}
	if(negative) num*=-1;
	printf("DEBUG: number=%d\n", num);

	// MEM: free char buffer
	free(charBuf);

	return true;
}

int main(void)
{
	int result;
	bool flag = str2int("adads-s-187123  2313", &result);

	printf("has dight chars %d\n", flag);

	return 0;
}