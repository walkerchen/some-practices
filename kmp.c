#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>
#include <math.h>
#include <memory.h>

void preKmp(char *x, int m, int* kmpNext) {
   int i, j;

   i = 0;
   j = kmpNext[0] = -1;
   while (i < m) {
   	  printf("Compare %c ? %c\n",  x[i], x[j]);
      while (j > -1 && x[i] != x[j])
         j = kmpNext[j];
      i++;
      j++;
      if (x[i] == x[j])
         kmpNext[i] = kmpNext[j];
      else
         kmpNext[i] = j;
   }
}

void calcStep(char *needle, int needleLen, int *steps)
{
	int step=0, mutualLen=0;

	for(int i=0; i<needleLen; i++)
	{
		// assign a default value of 0
		*(steps+i) = step;

		// calculate the max length of mutually owned sub-prefix and sub-postfix
		// Don't have both prefix and postfix if length less than 2
		for(int j=0; j<i; j++)
		{
			mutualLen = 0;
			for(int k=0; k<=j; k++)
			{
				//printf("Compare %c: %c ? %c\n",  *(needle+i), *(needle+k), *(needle+i-j+k));
				if(*(needle+k) == *(needle+i-j+k))
					mutualLen++;
				else
				{
					mutualLen = 0;
					break;
				}
				//printf("mutualLen=%d\n",  mutualLen);
			}
			//printf("-----------\n");

			if(mutualLen > (*(steps+i))) 
				*(steps+i) = mutualLen;
		}
	}
}

int kmpSearch(char *hay, char *needle, int *counter)
{
	char *pHay = hay, *pNeedle = needle;
	int hayLen = (int)strlen(hay);
	int needleLen = (int)strlen(needle);
	int *steps = (int *)malloc(sizeof(int)*needleLen);
	if(!steps) return -1;

	memset(steps, 0, sizeof(int)*needleLen);

	for(int i=0; i<needleLen; i++)
		printf("%d ", *(steps+i));
	printf("\n");

	calcStep(needle, needleLen, steps);
	//preKmp(needle, needleLen, steps);

	for(int i=0; i<needleLen; i++)
		printf("%d ", *(steps+i));
	printf("\n");

	while(*pHay)
	{
		pNeedle = needle;
		while(*pNeedle)
		{
			if(*pNeedle==*pHay)
			{
				printf("%c ? %c [%ld]\n", *pNeedle, *pHay, pHay-hay);
				pHay++;
				pNeedle++;
				(*counter)++;
				if(pNeedle-needle == needleLen)
				{
					free(steps);
					return (int)(pHay-hay-needleLen);
				}
			}
			else
			{
				printf("%c [%ld] not found\n", *pHay, pHay-hay);
				pHay++;
				pHay -= (pNeedle-needle) - steps[pNeedle-needle];
				//pHay++;
				(*counter)++;
				break;
			}
		}
	}


	free(steps);

	return -1;
}

void kmptest()
{
	//char hay[] = "12345_789_abcdef123_abcabxabc_567";
	//char needle[] = "abcabx";
	//char needle[] = "23";
	char hay[] = "BBC ABCDAB ABCDABCDABDEASDAD";
	char needle[] = "ABABAC";
	int counter=0;

	printf("%s\n", hay);
	printf("%s => skip table\n", needle);
	int pos = kmpSearch(hay, needle, &counter);
	printf("Position of needle %s in hay %s is %d. O(x)=%d.\n", needle, hay, pos, counter);

	//getchar();
}

int main(void)
{
	kmptest();
	return 0;
}
