// 2 4 8 3 0 1 5 6 9 7
// 1 0 0 0 6 2 0 1 0 0

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void hello()
{
	printf("Hello");
}

// linked list cross test
typedef struct LinkedListStru LinkedList;

struct LinkedListStru
{
	int payload;
	LinkedList *visitor;
	LinkedList *next;
	
};

void reVirgin(LinkedList *node)
{
	LinkedList *p = node;
	while(p->visitor!=NULL)
	{
		p->visitor=NULL;
		p = p->next;
	}
}

void printList(LinkedList *node)
{
	reVirgin(node);

	LinkedList *p = node, *prev = NULL;
	while(p!=NULL)
	{
		//printf("%d [%p]", p->payload, p->visitor);
		//printf("[%p]%d[%p]", p, p->payload, p->next);
		printf("%d", p->payload);
		if(p->visitor!=NULL)
		{
			printf("[CIRCULAR]");
			break;
		}
		p->visitor = prev;
		prev = p;
		p = p->next;
		if(p) printf("->");
	}
	printf("\n");

	reVirgin(node);
}

LinkedList* initList()
{
	LinkedList *base, *node;
	int size = rand()%10+5;
	base = (LinkedList *)malloc(size*sizeof(LinkedList));
	if(!base) return NULL;

	node = base;

	LinkedList *prev = NULL;
	printf("Allocate listed list with capacity of %d\n", size);
	for(int i=0; i<size; i++)
	{
		node->payload = rand()%100;
		node->visitor = NULL;
		node->next = NULL;
		if(prev)
			prev->next=node;
		prev = node;

		node++;
	}

	return base;
}

bool isCross(LinkedList *list1, LinkedList *list2)
{
	if(list1==NULL || list2==NULL) return false;

	while(list1->next != NULL)
		list1 = list1->next;
	while(list2->next != NULL)
		list2 = list2->next;

	return list1==list2;
}

bool checkCrossWithCircularPossibility(LinkedList *list1, LinkedList *list2)
{
	if(list1==NULL || list2==NULL) return false;

	LinkedList *visitor = list1;
	while(list1->next != NULL)
	{
		// If first visit
		if(list1->visitor==NULL) 
			list1->visitor = visitor;
		// Had been visited once, circular linked list
		else
			// Exit circular loop
			break;
		visitor = list1;
		list1 = list1->next;
	}

	visitor = list2;
	while(list2->next != NULL)
	{
		// If first visit
		if(list2->visitor==NULL) 
			list2->visitor = visitor;
		// Had been visited by previous node in same linked list, then is a circular linked list by itself, exit the loop
		else if(list2->visitor==visitor)
			break;
		// Had been visited by node in other linked list, then is cross with other.
		else 
			return true;
		visitor = list1;
		list1 = list1->next;
	}

	return list1==list2;
}

bool isCross2(LinkedList *list1, LinkedList *list2)
{
	reVirgin(list1);
	reVirgin(list2);

	bool result = checkCrossWithCircularPossibility(list1, list2);

	reVirgin(list1);
	reVirgin(list2);

	return result;
}

bool isCircular(LinkedList *list)
{
	if(list==NULL) return false;

	LinkedList *visitor = list;
	while(list->next != NULL)
	{
		// If first visit
		if(list->visitor==NULL) 
			list->visitor = visitor;
		// Had been visited once, circular linked list
		else
			// Exit circular loop
			return true;
		visitor = list;
		list = list->next;
	}

	return false;
}

void setCross(LinkedList *list1, LinkedList *list2)
{
	if(list1==NULL || list2==NULL) return;
	
	LinkedList *crossPoint = list1;
	while(list1->next != NULL)
	{
		if(rand()%3==0)
		{
			crossPoint = list1;
			break;
		}
		list1 = list1->next;
	}

	while(list2->next != NULL)
	{
		if(rand()%3==0)
		{
			list2->next = crossPoint;
			crossPoint = NULL;
			break;
		}
		list2 = list2->next;
	}

	if(crossPoint!=NULL)
		list2->next = crossPoint;
}

void setCircular(LinkedList *list)
{
	if(list==NULL) return;

	LinkedList *head = list;
	while(list->next != NULL)
	{
		list = list->next;
	}
	list->next = head;
}

void cleanMem(LinkedList *node)
{
	if(node)
		free(node);
}

void linkedListTest()
{
	LinkedList *list1, *list2;

	list1 = initList();
	list2 = initList();

	printf("Before cross:\n");
	printList(list1);
	printList(list2);

	//setCross(list1, list2);

	//bool result = isCross(list1, list2);
	//bool result = isCross2(list1, list2);

	setCircular(list1);
	setCross(list1, list2);
	//bool result = isCircular(list1);
	bool result = isCross2(list1, list2);

	printf("After cross: result: %d\n", result);
	printList(list1);
	printList(list2);

	cleanMem(list1);
	cleanMem(list2);
}

LinkedList *reverse(LinkedList *list)
{
	LinkedList *p=list, *prev = NULL, *temp;

	while(p!=NULL)
	{
		printf("%d,", p->payload);

		temp = p->next;
		if(prev!=NULL) p->next = prev;
		else p->next = NULL;

		prev=p;
		p=temp;
	}

	return prev;
}

void reverseTest()
{
	LinkedList *list1;

	list1 = initList();

	printList(list1);	
	list1 = reverse(list1);
	printf("\n");
	printList(list1);	
}

int main(void)
{
	srand( (unsigned)time( NULL ) );

	//linkedListTest();
	reverseTest();
	return 0;
}