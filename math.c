#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>
#include <math.h>
#include <memory.h>

char *format_binary(int num)
{
	int pad=8;
	char *str = (char *)malloc(sizeof(char) * (pad+1));
  	if (str) 
  	{
   		str[pad]='\0';
   		while (--pad>=0) 
   		{
    		str[pad] = num & 1 ? '1' : '0';
    		num >>= 1;
   		}
  	} else 
  	{
   		return (char*)"";
  	}
 	return str;
}

int add_bit(int a, int b)
{
	int d1, d2, d;
	int carry = 0;
	int sum = 0;

	for(int i=0; i<sizeof(int)*8; i++)
	{
		d1 = (a&(1<<i))?1:0;
		d2 = (b&(1<<i))?1:0;

		d = (d1+d2+carry) & 1;
		carry = ((d1+d2+carry) & (1<<1)) >> 1;

		printf("%d", d);

		sum+=d<<i;
	}

	return sum;
}

int add(int x, int y)
{
	if (y == 0)
        return x;
    else
        return add( x ^ y, (x & y) << 1);
}

int last1(int n)
{
	return n & (0-n);
}

int removeLast1(int n)
{
	return n & (n-1);
}

int add_by_bit(int a, int b)
{
	int result = 0;
	for(int i=0; i<sizeof(int)*8; i++)
		result += add(a&(1<<i), b&(1<<i));

	return result;
}

int mul(int a, int b)
{
	int result = 0, remain;
	int i;
	remain = a;

	for(i=sizeof(int)*8-1; i>=0; i--)
	{
		if((1<<i)-remain<=0)
		{
			remain -= (1<<i);
			result += b<<i;
		}
	}

	//printf("\n");

	return result;
}

int sum(int n)
{
	return ((n<<1)+n)/2;
}

int joseph(int n, int m) {
	int fn=0;
	for (int i=2; i<=n; i++) {
	fn = (fn+m)%i; } return fn;
}

int main(void)
{
	int n=9;

	//printf("%d %d %d", -9, ~(9-1), ~9+1);

	printf("%d [%s]: lastBinDigh=%s, removeLast1=%s\n", 
		n, format_binary(n), format_binary(last1(n)), format_binary(removeLast1(n)));

	printf("sum=%d\n", add(9, 10));
	printf("mul=%d\n", mul(127, 12));
	printf("joseph=%d\n", joseph(698, 3));
	return 0;
}
