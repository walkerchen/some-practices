#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include <memory.h>
#include <limits.h>

bool maxSubArray(int* arr, int size, int arrLength, int *max)
{
	*max = INT_MIN;

	printf("Array length=%d, given size=%d\n", arrLength, size);
	if(size<=0 || size>arrLength) 
		return false;

	int* pArr = arr;
	int sum = 0;

	while(true)
	{
		printf("item = %d\n", *pArr);
		sum += *pArr++;
		printf("sum = %d\n", sum);
		if(pArr-arr>=size)
		{
			if(sum>*max) *max=sum;
			sum -= *(pArr-size);
		}
		if(pArr>=arr+arrLength) break;
	}

	return true;
}

int main(void)
{
	int arr[] = {1, -2, 3, 10, -4, 7, 2, -5};
	int maxValue;
	int arrLength = sizeof(arr)/sizeof(int);

	bool result = maxSubArray(arr, 5, arrLength, &maxValue);

	printf("Result is %s, max sum=%d\n", result?"true":"false", maxValue);

	return 0;
}

