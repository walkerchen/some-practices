#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include <memory.h>
#include <limits.h>

/*
取出大小为length的数组的最小K个元素

1、取前k个元素指针存入指针数组 minArr
2、遍历剩下的length-k个元素，如果该元素比 minArr 的数小，则找到 minArr 中最大的数，并替换成该元素

结果以指针数组的形式返回。
*/
bool minK(int *arr, int k, int arrLength, int **&minArr)
{
	if(k>arrLength) return false;

	int minArrSize = sizeof(int *)*k;

	minArr = (int **)malloc(minArrSize);
	if(!minArr) return false;

	printf("arr=%p, arrLength=%d, minArr=%p, size=%d, k=%d\n", arr, arrLength, minArr, minArrSize, k);

	memset(minArr, minArrSize, NULL);

	int **t;
	for(int i=0; i<arrLength; i++)
	{
		if(i<k)
		{
			*(minArr+i) = (arr+i);
			printf("init %d with pointer to %p [%d], result=%p\n", i, arr+i, **(minArr+i), *(minArr+i));
			continue;
		}

		t = NULL;
		for(int j=0; j<k; j++)
		{
			if(**(minArr+j)>*(arr+i))
			{
				if(!(t))
				{
					t = &(*(minArr))+j;
				}
				else if(**(minArr+j)>**t)
				{
					t = &(*(minArr))+j;
				}
				//printf("Lagest number %p [%d] %p\n", temp, *temp, t );
			}
		}
		if(t)
		{
			printf("Replace %d with %d\n",  **t, *(arr+i));
			*t = arr+i;
		}
	}

	return true;
}

int main(void)
{
	int arr[] = {1, -2, 3, 10, -4, 7, 2, -5};
	int **minArr;
	int k=5;
	int arrLength = sizeof(arr)/sizeof(int);

	bool result = minK(arr, k, arrLength, minArr);

	printf("Result is %s\n", result?"true":"false");
	if(result)
	{
		for(int i=0; i<arrLength; i++)
			printf("%d ", *(arr+i));
		printf("\n");

		for(int i=0; i<k; i++)
			printf("%d ", **(minArr+i));
		printf("\n");
	}

	if(minArr)
		free(minArr);

	return 0;
}

