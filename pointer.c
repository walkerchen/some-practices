#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include <memory.h>

typedef struct 
{
	int data;
}STRU;

int main(void)
{
	char str[] = "Hello";
	char *p;
	char **pp;

	p=str;

	pp=&p;

	printf("Str=%s[%p], p=%s value[%p] addr[%p], *p=%c, pp=%s[%p] [%p] [%c]\n", str, str, p, p, &p, *p, *pp, *pp, pp, **pp);
	p++;
	printf("Str=%s[%p], p=%s value[%p] addr[%p], *p=%c, pp=%s[%p] [%p] [%c]\n", str, str, p, p, &p, *p, *pp, *pp, pp, **pp);
	*p='*';
	*(*pp+1) = '#';
	printf("Str=%s[%p], p=%s value[%p] addr[%p], *p=%c, pp=%s[%p] [%p] [%c] [%p]\n", str, str, p, p, &p, *p, *pp, *pp, pp, **pp, *pp+1);

	STRU s;
	STRU *sp;

	return 0;
}