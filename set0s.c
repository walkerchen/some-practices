#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>
#include <math.h>
#include <memory.h>
#include <assert.h>

int *initMatrix(size_t m, size_t n)
{
	int *matrix = (int *)malloc(sizeof(int)*m*n);
	for(int i=0; i<m*n; i++)
	{
		*(matrix+i) = rand() % 50;
	}
	return matrix;
}

void freeMatrix(int *matrix)
{
	if(NULL != matrix)
		free(matrix);
}

void setZeros(int *matrix, int m, int n, int tm, int tn)
{
	for(int i=0; i<=m; i++)
	{
		for(int j=0; j<n; j++)
		{
			if(i==tm || j==tn) 
				*(matrix+i*n+j) = -1;
		}
	}
}

void allZeros(int *matrix, size_t m, size_t n)
{
	if(NULL == matrix) return;

	for(int i=0; i<m; i++)
	{
		for(int j=0; j<n; j++)
		{
			if(*(matrix+i*n+j)==0)
			{
				//printf("Zeroing %d %d\n", i, j);
				setZeros(matrix, m, n, i, j);
			}
		}
	}
	
	for(int i=0; i<m*n; i++)
	{
		if(*(matrix+i)==-1)
			*(matrix+i) = 0;
	}}

void dispMatrix(int *matrix, size_t m, size_t n)
{
	if(NULL == matrix) return;

	for(int i=0; i<m; i++)
	{
		for(int j=0; j<n; j++)
			printf("%3d ", *(matrix+i*n+j));
		printf("\n");
	}
}

int main(void)
{
	srand(time(NULL));
	int *matrix;
	int m=8, n=9;

	matrix = initMatrix(m, n);
	dispMatrix(matrix, m, n);
	allZeros(matrix, m, n);
	printf("\n\n");
	dispMatrix(matrix, m, n);

	return 0;
}