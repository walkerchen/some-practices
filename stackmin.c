#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include <memory.h>

#define CAPACITY 128

typedef struct ElementStruc
{
	int value;
}Element;

typedef struct StackStruc
{
	int top;
	int capacity;
	Element *data;
	Element *minElement;
}Stack;

void init(Stack *&stack, int cap)
{
	stack = (Stack *)malloc(sizeof(Stack));
	stack->capacity = cap;
	stack->top = -1;
	stack->data = (Element *)malloc(sizeof(Element *)*cap);
}

bool push(Element *elem, Stack *&stack)
{
	if(stack->top >= stack->capacity) return false;

	if(stack->top == -1)
		stack->minElement = elem;
	else
		if(stack->minElement->value > elem->value)
			stack->minElement = elem;

	stack->top++;
	Element *elemPtr = stack->data + stack->top;
	//printf("data=%p, top=%p, elemPtr=%p\n", stack->data, stack->data+stack->top, elemPtr);
	*elemPtr = *elem;

	return true;
}

Element *pop(Stack *&stack)
{
	if(stack->top<0) return NULL;
	stack->top--;
	Element *topElem = stack->data + stack->top;

	Element *elemPtr = topElem;
	stack->minElement = elemPtr;
	//printf("===============\n");
	for(int i=stack->top; i>=0; i--)
	{
		//printf("Pvalue=%d, min=%d\n", elemPtr->value, stack->minElement->value);
		if(elemPtr->value < stack->minElement->value)
		{
			stack->minElement = elemPtr;
		}

		elemPtr--;
	}

	return topElem;
}

Element *getMinElement(Stack *stack)
{
	return stack->minElement;
}

void detor(Stack *stack)
{
	if(stack)
		free(stack);
}

int main(void)
{
	Stack *stack;
	int testSize = 6;
	Element elements[testSize];

	srand(time(NULL));

	init(stack, CAPACITY);

	for(int i=0; i<testSize; i++)
	{
		elements[i].value = rand() % 100;
		push(&elements[i], stack);
		printf("%d = %d\n", i, elements[i].value);
	}

	Element *minElement = getMinElement(stack);
	printf("Minimum element of stack is %d\n",  minElement->value);

	for(int i=0; i<testSize/2; i++)
		pop(stack);

	minElement = getMinElement(stack);
	printf("Minimum element of stack is %d\n",  minElement->value);

	detor(stack);
	return(0);
}
