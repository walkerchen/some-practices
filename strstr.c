#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>
#include <math.h>
#include <memory.h>

/**
 * each value of array rollback means: when source[i] mismatch pattern[i],
 * KMP will restart match process form rollback[j] of pattern with
 * source[i]. And if rollback[i] == -1, it means the current source[i] will
 * never match pattern. then i should be added by 1 and j should be set to
 * 0, which means restart match process from source[i+1] with pattern from
 * pattern[0].
 * 
 * @param pattern
 * @return
 */
int* getRollbackArray(char* pattern, int len) 
{
    int *rollback = (int *)malloc(len * sizeof(int));
    for (int i = 0; i < len; i++) {
        rollback[i] = 0;
    }
    rollback[0] = -1;
    for (int i = 1; i < len; i++) {
        char prevChar = pattern[i - 1];
        int prevRollback = i - 1;
        while (prevRollback >= 0) {
            int previousRollBackIdx = rollback[prevRollback];
            if ((previousRollBackIdx == -1)
                    || (prevChar == pattern[previousRollBackIdx])) {
                rollback[i] = previousRollBackIdx + 1;
                break;
            } else {
                prevRollback = rollback[prevRollback];
            }
        }
    }
    return rollback;
}

int ac_strstr(char *str, char *substr)
{
	if(str==NULL || substr==NULL) return -1;
	else if(substr==NULL || strlen(substr)==0) return 0;

	int pos = -1;
	char *strp = str, *substrp = substr;
	char *piece;

	while(*strp)
	{
		substrp = substr;
		while(*substrp)
		{
			piece = (strp + (substrp - substr));
			//printf("Compare %c to %c\n", *piece, *substrp);
			if(*(piece)==*substrp)
			{
				if(substrp==substr)
					pos = strp-str;
			}
			else
			{
				pos = -1;
				break;
			}
			//printf("Found %c to %c, pos=%d\n", *piece, *substrp, pos);
			substrp++;
		}
		if(pos!=-1) return pos;
		strp++;
	}
	return pos;
}

int ac_faststrstr(char *str, char *substr)
{
	int subLen = strlen(substr);
	int len = strlen(str);
	if(str==NULL || substr==NULL) return -1;
	else if(substr==NULL || subLen==0) return 0;

	int pos = -1, nextStep;
	char *strp = str, *substrp = substr;
	char *piece;

	int *arrRollback = getRollbackArray(substr, subLen);
	if(arrRollback==NULL) return -1;

	for(int i=0; i<subLen; i++)
		printf("Char %c=%d\n", *(substr+i), *(arrRollback+i));

	int currMatch = 0;

	int i=0;
	while(!(str+i))
	{
		if ((currMatch == -1) || (str[i] == substr[currMatch])) 
		{
			i++;
			currMatch++;
			if (currMatch == len) 
			{
				return i - len;
            }
		} else {
			currMatch = arrRollback[currMatch];
		}
	}

	free(arrRollback);

	return pos;
}


int main(void)
{
	int pos;
	
	char str[] 		= "cab_bababxabc_ababxababxababx";
	char substr[]	= "ababxababxababx";
	pos = ac_faststrstr(str, substr);

	printf("Pos of %s in %s is %d\n", substr, str, pos);

	return 0;
}