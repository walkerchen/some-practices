#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>
#include <math.h>
#include <memory.h>
#include <assert.h>

#define DEBUG

#ifdef DEBUG
void printIntAsBit(int l)
{
	for(int i=0; i<sizeof(int)*8; i++)
	{
		printf("%s", (l>>(sizeof(int)*8-1-i))&1?"1":"0");
		if(i%2==1) printf(" ");
	}
	printf("\n");
	return;
}
#endif

/*
Test if a all characters in a string pstr are all unique. Assume all characters were writtern in lowercase.

parameters:
pstr: The pointer to the input string
result:
A boolean value to indicate wether the string have uniques characters or not.
*/
bool isAllUnique(char *pstr)
{
	int testBed = 0;
	int testBit = 0;

	while('\0' != *pstr)
	{
#ifdef DEBUG
		printf("%c\n", *pstr);
#endif
		testBit = 1<<(*pstr-'a');
		if(testBed & testBit) return false;
		else testBed = testBed | testBit;
#ifdef DEBUG
		printIntAsBit(testBed);
#endif
		pstr++;
	}
	return true;
}

int revOddEven(int iin)
{
	return ((iin&0xaaaaaaaa)>>1) | ((iin&0x55555555)<<1);
}

int main(void)
{
	//char str[] = "thequickfoxlazydog";
	//bool result = isAllUnique(str);
	//printf("Test result of '%s' is %s.\n", str, result?"true":"false");

	srand(time(NULL));
	int ir = rand();
	printIntAsBit(ir);
	ir = revOddEven(ir);
	printIntAsBit(ir);
	printIntAsBit(0xaaaaaaaa);
	printIntAsBit(0x55555555);

	return 0;
}