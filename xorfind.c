#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>
#include <math.h>
#include <memory.h>

#define ARR_SIZE 11
void xorfind()
{
	int sample[ARR_SIZE];
	int hiddenNumber = 8;

	for(int i=0; i<ARR_SIZE-1; i++)
	{
		sample[i] = i+1;
	}
	sample[ARR_SIZE-1] = hiddenNumber;

	for(int i=0; i<ARR_SIZE; i++)
	{
		printf("%d ", sample[i]);
	}
	printf("\n");

	int needle=0;

	for(int i=0; i<ARR_SIZE; i++)
	{
		if(i==0) 
			needle=sample[i];
		else
			needle ^= needle^sample[i];
	}

	printf("needle=%d [expected to be %d]\n", needle, hiddenNumber);
}

int main(void)
{
	xorfind();
	return 0;
}
